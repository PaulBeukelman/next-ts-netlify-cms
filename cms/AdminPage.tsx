import React from 'react';
import cms from 'netlify-cms-app';
import { config } from './config';

const AdminPage: React.FC = () => {
  React.useEffect(() => {
    cms.init({ config });
  }, []);

  return null;
};
export default AdminPage;
