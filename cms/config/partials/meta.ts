import { CmsField } from 'netlify-cms-core';

export const metaFields: CmsField[] = [
  {
    label: 'Meta',
    name: 'meta',
    widget: 'object',
    fields: [
      {
        label: 'Title',
        name: 'title',
        widget: 'string',
        required: false,
      },
      {
        label: 'Description',
        name: 'desc',
        widget: 'text',
        required: false,
      },
      {
        label: 'Slug',
        name: 'slug',
        widget: 'string',
        required: false,
      },
      {
        label: 'Featured image',
        name: 'image',
        widget: 'image',
        required: false,
      },
    ],
  },
];
