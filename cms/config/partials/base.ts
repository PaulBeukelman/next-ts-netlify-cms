import { CmsField } from 'netlify-cms-core';

export const baseFields: CmsField[] = [
  { label: 'Title', name: 'title', widget: 'string' },
  { label: 'Content', name: 'body', widget: 'markdown' },
];
