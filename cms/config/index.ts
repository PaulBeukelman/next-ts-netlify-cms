import { CmsConfig } from 'netlify-cms-core';
import { baseFields } from './partials/base';
import { metaFields } from './partials/meta';

export const config: CmsConfig = {
  backend: {
    name: 'git-gateway',
  },
  media_folder: 'public/img',
  public_folder: 'img',
  collections: [
    {
      name: 'posts',
      label: 'Posts',
      folder: 'content/posts',
      create: true,
      slug: '{{fields.meta.slug}}',
      fields: [...baseFields, ...metaFields],
    },
    {
      name: 'pages',
      label: 'Pages',
      folder: 'cms/content',
      create: true,
      slug: '{{fields.meta.slug}}',
      fields: [...baseFields, ...metaFields],
    },
  ],
};
