interface LoggerArgs {
  message: string;
  type?: 'info' | 'error';
  data?: unknown;
}

export const logger = ({ message, type = 'info', data }: LoggerArgs): void => {
  const icon = type === 'info' ? '☝' : '☠';
  let logBody = `${icon}  ${type} - ${message}`;
  if (data) {
    logBody += ` - ${JSON.stringify(data)}`;
  }
  // eslint-disable-next-line
  console.log(logBody);
};
