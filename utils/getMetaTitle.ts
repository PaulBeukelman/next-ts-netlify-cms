import { config } from '../site-config';

export const getMetaTitle = (names: (string | undefined)[]): string => {
  const pageName = (names || []).filter(Boolean).join(' - ');
  return [pageName, config.meta.title].join(' | ');
};
