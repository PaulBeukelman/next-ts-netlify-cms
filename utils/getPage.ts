import { promises as fs } from 'fs';
import matter, { Input } from 'gray-matter';
import path from 'path';
import { Resource } from '../typings';
import { getMetaTitle } from './getMetaTitle';
import { logger } from './logger';

interface GetPageParams {
  filename: string;
  dir?: string;
}

export const getPage = async ({ filename, dir }: GetPageParams): Promise<null | Resource> => {
  let mdFile: null | { default: Input } = null;
  const originalPath = [dir, filename].filter(Boolean).join('/');

  // first we try to find the page by path and filename
  try {
    mdFile = await import(`../cms/content/${originalPath}`);
  } catch {
    logger({ message: `Failed loading file ${originalPath}` });
  }

  if (mdFile) {
    // We got a matching file, now map all values to a Resource format
    const {
      content,
      data: { meta, date, ...rest },
    } = matter(mdFile.default);
    const slug = encodeURIComponent((meta?.slug || filename).replace('.md', ''));
    return Promise.resolve({
      dir: dir || 'root',
      filename,
      frontmatter: rest,
      body: content,
      meta: {
        ...meta,
        date,
        slug,
        title: getMetaTitle([meta?.title]),
        path: `/${[dir, slug].filter(Boolean).join('/')}`,
      },
    });
  }

  // We are probably dealing with an custom slug, so we need to get all pages and filter out the right slug.
  const pages = await getPages(dir);
  return pages.find((page) => page.meta.slug === filename) || null;
};

export const getPages = async (dir?: string): Promise<Resource[]> => {
  const pathToDir = path.join(process.cwd(), `cms/content${dir ? '/' + dir : ''}`);
  const filenames = await fs.readdir(pathToDir);
  const mdFileNames = filenames.filter((file) => file.endsWith('.md'));
  return Promise.all(
    mdFileNames.map((filename) => {
      return getPage({ filename, dir });
    })
  );
};
