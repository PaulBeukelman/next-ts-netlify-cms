with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "next-ts-netlify-cms";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    nodejs-16_x
  ];
}

