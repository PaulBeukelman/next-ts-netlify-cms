import Link from 'next/link';
import { PostResource } from '../typings';

interface Props {
  posts: PostResource[];
}

export const PostList: React.FC<Props> = ({ posts }) => {
  if (!posts.length) {
    return <div>No posts!</div>;
  }
  return (
    <div>
      <ul>
        {posts.map((post) => {
          return (
            <li key={post.meta.path}>
              {post.meta.date}: {` `}
              <Link href={{ pathname: `${post.meta.path}` }}>
                <a>{post?.frontmatter?.title}</a>
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
