export const Loader: React.FC = () => {
  return <div className="flex items-center justify-center">Loading...</div>;
};
