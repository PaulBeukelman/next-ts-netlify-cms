import Head from 'next/head';
import Link from 'next/link';
import React from 'react';
import { Meta } from '../typings';
import { Footer } from './Footer';

interface Props {
  meta: Meta;
  sidebar?: React.ReactNode;
}

export const Layout: React.FC<Props> = ({ meta, sidebar, children }) => {
  return (
    <div className="container">
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.desc} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <nav>
        <Link href="/">
          <a>Home</a>
        </Link>
        <Link href="/posts">
          <a>Posts</a>
        </Link>
      </nav>
      {sidebar && (
        <div className={'md:flex'}>
          <main>{children}</main>
          <aside>{sidebar}</aside>
        </div>
      )}
      {!sidebar && <main>{children}</main>}
      <Footer />
    </div>
  );
};
