import Link from 'next/link';
import ReactMarkdown from 'react-markdown';
import { Layout } from '../../components/Layout';
import { PostResource, StaticProps } from '../../typings';
import { getPage, getPages } from '../../utils/getPage';

type Props = PostResource;

const BlogPost: React.FC<Props> = (props) => {
  return (
    <Layout meta={props.meta}>
      <Link href="/posts">
        <a>Back to post list</a>
      </Link>
      <article>
        <h1>{props.frontmatter.title}</h1>
        <div>
          <ReactMarkdown>{props.body}</ReactMarkdown>
        </div>
      </article>
    </Layout>
  );
};

export default BlogPost;

export const getStaticProps = async (ctx): Promise<StaticProps<Props>> => {
  const { slug } = ctx.params;
  const post = (await getPage({ filename: slug, dir: 'posts' })) as Props | null;
  if (!post) {
    if (!post) {
      return {
        notFound: true,
      };
    }
  }
  return {
    props: post,
  };
};

export const getStaticPaths = async (): Promise<{ paths: string[]; fallback: boolean }> => {
  const pages = await getPages('posts');
  return {
    paths: pages.map((page) => page.meta.path),
    fallback: false,
  };
};
