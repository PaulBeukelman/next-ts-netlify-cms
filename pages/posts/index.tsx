import { Layout } from '../../components/Layout';
import { PostList } from '../../components/PostList';
import { List, PostResource, StaticProps } from '../../typings';
import { getMetaTitle } from '../../utils/getMetaTitle';
import { getPages } from '../../utils/getPage';

type Props = List<PostResource>;

const Index: React.FC<Props> = (props) => {
  return (
    <Layout meta={props.meta}>
      <h1>{props.title}</h1>
      <PostList posts={props.resources} />
    </Layout>
  );
};

export default Index;

export const getStaticProps = async (): Promise<StaticProps<Props>> => {
  const pages = (await getPages('posts')) as PostResource[];

  return {
    props: {
      title: 'Blog',
      resources: pages,
      meta: {
        title: getMetaTitle(['Blog']),
      },
    },
  };
};
