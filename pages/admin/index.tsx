import React from 'react';
import dynamic from 'next/dynamic';
import { Loader } from '../../components/Loader';
import { StaticProps } from '../../typings';

const handleLoading = (): JSX.Element => <Loader />;

const DynamicAdminPage = dynamic(() => import('../../cms/AdminPage'), {
  ssr: false,
  loading: handleLoading,
});

const AdminPage: React.FC = () => {
  return <DynamicAdminPage />;
};

export default AdminPage;

export const getStaticProps = async (): Promise<StaticProps<Record<string, unknown>>> => {
  return {
    props: {},
  };
};
