import '../styles/global.css';

// eslint-disable-next-line
const Application = ({ Component, pageProps }) => {
  return (
    <div className="main-wrapper">
      <Component {...pageProps} />
    </div>
  );
};

export default Application;
