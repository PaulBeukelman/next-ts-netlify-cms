import ReactMarkdown from 'react-markdown';
import { Layout } from '../components/Layout';
import { HomePageResource } from '../typings';
import { getPage } from '../utils/getPage';

type Props = HomePageResource;

const Index: React.FC<Props> = (props) => {
  return (
    <Layout meta={props.meta}>
      <h1>{props.frontmatter.title}</h1>
      <ReactMarkdown>{props.body}</ReactMarkdown>
    </Layout>
  );
};

export default Index;

export const getStaticProps = async (): Promise<{ props: Props }> => {
  const homePage = (await getPage({ filename: 'index' })) as HomePageResource;
  return {
    props: homePage,
  };
};
