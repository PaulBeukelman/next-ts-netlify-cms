import { BaseFields, Resource } from '../..';

export type Post = BaseFields;

export type PostResource = Resource<'posts', Post>;
