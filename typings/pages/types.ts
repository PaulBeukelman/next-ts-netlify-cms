import { BaseFields, Resource } from '..';
import { PostResource } from './posts/types';

// convert to interface and extend
export type HomePage = BaseFields;
export type HomePageResource = Resource<'root', HomePage>;

export type PageResource = HomePageResource | PostResource;
