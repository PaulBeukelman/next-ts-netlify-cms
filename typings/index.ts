export interface BaseFields {
  title: string;
}

/**
 * Some basic stuff usefull for SEO in the head
 */
export interface Meta {
  title?: string;
  desc?: string;
  image?: string;
  date?: string;
  slug?: string;
  path?: string;
}

/**
 * Resource to build single view
 */
export interface Resource<D = string, FM = unknown> {
  /**
   * Which directory the file lives
   */
  dir: D;
  /**
   * Actual file name
   */
  filename: string;
  /**
   * All none meta or body fields
   */
  frontmatter: FM;
  /**
   * Body content
   */
  body: string;
  /**
   * All meta/SEO related fields
   */
  meta: Meta;
}

/**
 * List of resources to build list views
 */

export interface List<R> {
  title: string;
  resources: R[];
  meta: Meta;
}

export interface StaticProps<P> {
  notFound?: boolean;
  props?: P;
}

export * from './pages/types';
export * from './pages/posts/types';
