# Next + Netlify CMS + TypeScript

This is a [Next.js](https://nextjs.org/) v10.1 project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). It is an extended TypeScript version of this [repository](https://github.com/cassidoo/next-netlify-starter) and it comes with TailwindCSS and some opinionated basics.

## Local development

Install deps

```bash
npm i
# or
yarn
```

Run the development server:

```bash
npm run start
# or
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Project overview

- `/cms` holds all config and content for Netlify CMS
- `/components` reusable components that can be loaded into pages
- `/pages` Here you will find all page templates or static pages.
- `/public` Folder that will be copied 1:1 to your website's root
- `/styles` Global styling
- `/typings` All types live here
- `/utils` All utils live here

### Installation options

Fork or copy this repo, and click the button below to setup netlify, select the fork and you are ready to go.

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start)
